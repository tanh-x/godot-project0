package player

import godot.AnimationNodeStateMachinePlayback
import godot.AnimationPlayer
import godot.AnimationTree
import godot.Input
import godot.KinematicBody2D
import godot.annotation.RegisterClass
import godot.annotation.RegisterFunction
import godot.core.Vector2
import utils.get
import utils.node
import kotlin.math.log

/**
 * The Player class
 */
@RegisterClass
class Player : KinematicBody2D() {
	/**
	 * The [AnimationPlayer] node of the player
	 */
	internal lateinit var animPlayer: AnimationPlayer private set

	/**
	 * The [AnimationTree] node of the player
	 */
	internal lateinit var animTree: AnimationTree private set

	/**
	 * The state of the [AnimationTree] node's FSM
	 */
	internal lateinit var animState: AnimationNodeStateMachinePlayback private set

	/**
	 * The *unit* [Vector2] that stores the current velocity of the player, which [KinematicBody2D]
	 * doesn't supply. We keep track of this, then feed it into [KinematicBody2D.moveAndSlide]
	 * in [PlayerState.DEFAULT.stateFunction]
	 */
	internal val velocity: Vector2 = Vector2.ZERO

	/**
	 * The current state of the player. The implementation of most actions that the player can do
	 * is implemented within the enum's anonymous classes
	 */
	private var currentState: PlayerState = PlayerState.DEFAULT
		set(value) {
			field = value
			field.onSwitch(this)
		}

	@RegisterFunction
	override fun _ready() {
		animPlayer = node("AnimationPlayer")
		animTree = node("AnimationTree")
		animTree.active = true
		animState = animTree["parameters/playback"]
	}

	@RegisterFunction
	override fun _process(delta: Double) {
		currentState.stateFunction(this, delta)
		moveAndSlide(velocity * DFLT_VELOCITY_SCALAR)
	}

	/**
	 * Manually triggers a transition of [currentState]. Useful for animation keying.
	 */
	@RegisterFunction
	fun triggerStateTransition() {
		currentState = currentState.transitionFunction(this)
	}

	internal fun getInputVector(): Vector2 = Vector2(
		x = Input.getActionStrength("ui_right") - Input.getActionStrength("ui_left"),
		y = Input.getActionStrength("ui_down") - Input.getActionStrength("ui_up")
	).normalized()

	companion object {
		/**
		 * The default movement speed of the player, probably in units per second
		 */
		internal const val DFLT_VELOCITY_SCALAR: Double = 170.0

		/**
		 * The "t" argument when we linearly interpolate the velocity with the new "input vector",
		 * this creates a feel for "acceleration". More concretely, this number is the scalar in
		 * the differential equation y' = tΔv, where Δv = (input vector) - (current velocity)
		 */
		internal const val VELOCITY_EXP_INTERP_T: Double = 0.311

		/**
		 * Like above, but for stopping when the player attacks.
		 */
		internal const val VELOCITY_EXP_INTERP_ATTACK: Double = 0.195
	}
}