package player

import godot.Input
import godot.core.Vector2
import godot.global.GD
import utils.FiniteStateMachine
import utils.getBlendPositionOf
import utils.setBlendPositionOf

/**
 * A [FiniteStateMachine] that represents the states of the [Player]
 */
internal enum class PlayerState : FiniteStateMachine<Player> {
	/**
	 * The [DEFAULT] state includes idling and running.
	 * Transitions to [ATTACK] or [ROLL] when certain keys are pressed.
	 */
	DEFAULT {
		override fun stateFunction(player: Player, delta: Double): Unit = with(player) {
			val inputVec: Vector2 = getInputVector()

			if (inputVec.x != 0.0 || inputVec.y != 0.0) {
				animTree.setBlendPositionOf("RunSpace", inputVec)
				animTree.setBlendPositionOf("IdleSpace", inputVec)
				animState.travel("RunSpace")
			} else {
				animState.travel("IdleSpace")
			}

			velocity.x = GD.lerp(velocity.x, inputVec.x, Player.VELOCITY_EXP_INTERP_T)
			velocity.y = GD.lerp(velocity.y, inputVec.y, Player.VELOCITY_EXP_INTERP_T)

			triggerStateTransition()
		}

		override fun transitionFunction(obj: Player): PlayerState {
			if (Input.isActionJustPressed("attack")) return ATTACK
			return DEFAULT
		}
	},

	/**
	 * The [ATTACK] state handles the attack animation.
	 * Transitions back to the [DEFAULT] state upon completion
	 */
	ATTACK {
		override fun onSwitch(player: Player): Unit = with(player) {
			animState.travel("AttackSpace")
			animTree.setBlendPositionOf("AttackSpace", animTree.getBlendPositionOf("IdleSpace"))
		}

		override fun stateFunction(player: Player, delta: Double): Unit = with(player) {
			velocity.x *= 1 - Player.VELOCITY_EXP_INTERP_ATTACK
			velocity.y *= 1 - Player.VELOCITY_EXP_INTERP_ATTACK
		}

		override fun transitionFunction(obj: Player): PlayerState = DEFAULT
	},

	/**
	 * The [ROLL] state handles rolling capabilities.
	 * Transitions back to the [DEFAULT] state upon completion
	 */
	ROLL {
		override fun stateFunction(player: Player, delta: Double): Unit = with(player) {

		}
	};

	/**
	 * The functionality we want the player to carry out in each state at each timestep.
	 *
	 * @param player The player
	 * @param delta Physics engine's delta time
	 */
	internal abstract fun stateFunction(player: Player, delta: Double)

	/**
	 * Gets called when the state is first transitioned onto.
	 *
	 * @param player The player
	 */
	internal open fun onSwitch(player: Player) {}

	open override fun transitionFunction(obj: Player): PlayerState = this
}
