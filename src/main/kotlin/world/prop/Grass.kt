package world.prop

import godot.Input
import godot.Node2D
import godot.annotation.RegisterClass
import godot.annotation.RegisterFunction

@RegisterClass
class Grass : Node2D() {
	@RegisterFunction
	override fun _ready() {

	}

	@RegisterFunction
	override fun _process(delta: Double) {
		if (Input.isActionJustPressed("Attack")) {
			queueFree()
		}
	}

}