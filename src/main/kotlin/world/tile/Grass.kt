package world.tile

import godot.Area2D
import godot.Input
import godot.Node2D
import godot.annotation.RegisterClass
import godot.annotation.RegisterFunction
import utils.instantiateScene

@RegisterClass
class Grass : Node2D() {
	@RegisterFunction
	fun onAreaEntered(area: Area2D) {

		instantiateScene<GrassFX>("res://GrassFX.tscn").let { fx: GrassFX ->
			fx.globalPosition = this.globalPosition
			getParent()!!.addChild(fx)
		}
		queueFree()
	}
}