package world.tile

import godot.AnimatedSprite
import godot.Node2D
import godot.annotation.RegisterClass
import godot.annotation.RegisterFunction
import utils.node

@RegisterClass
class GrassFX : Node2D() {
	@RegisterFunction
	override fun _ready() {
		node<AnimatedSprite>("AnimatedSprite").play("Animate")
	}

	@RegisterFunction
	fun onAnimationFinished() {
		queueFree()
	}
}